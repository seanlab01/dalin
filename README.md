# 파이어 베이스 머신러닝
A collection of real life apps built using [Firebase ML Kit](https://firebase.google.com/products/ml-kit/) APIs.

# 셋업 순서

1. 프로젝트 복사하기
2. 파이어베이스 콘솔에 추가하기
3. 앱 인스톨 하기


# 스크린 샷 : 
![image01](https://raw.githubusercontent.com/the-dagger/MLKitAndroid/master/art/screen01.png)

# Libraries

* [CameraKit Android](https://github.com/CameraKit/camerakit-android)
* [FAB Progress to show progress](https://github.com/JorgeCastilloPrz/FABProgressCircle)
* [Picking Image from camera/gallery](https://github.com/jkwiecien/EasyImage)

Built with ❤️ by ksh ([@the-dagger](https://github.com/the-dagger)

# License
1.Machine Learning Kit
License
© Google, 2019. Licensed under an Apache-2 license.

2. Ghost app
Copyright & License
Copyright (c) 2013-2019 Ghost Foundation - Released under the MIT license. Ghost and the Ghost Logo are trademarks of Ghost Foundation Ltd. Please see our trademark policy for info on acceptable usage.

3.Camera Kit 
CameraKit is MIT License

4. Chaquopy
GNU General Public License V3.0
