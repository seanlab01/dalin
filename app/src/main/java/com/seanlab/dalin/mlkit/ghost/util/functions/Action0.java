package com.seanlab.dalin.mlkit.ghost.util.functions;

/**
 * A zero-argument action.
 */
public interface Action0 {

    void call();

}
